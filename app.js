const form = document.querySelector("#todo-form");
const todoInput = document.querySelector("#todo");
const todoList = document.querySelector(".list-group");
const firstCardBody = document.querySelectorAll(".card-body")[0];
const secondCardBody = document.querySelectorAll(".card-body")[1];
const filter = document.querySelector("#filter");
const clearbutton = document.querySelector("#clear-todos");

eventListeners();

function eventListeners() {  //Tüm event Listenerlar
    form.addEventListener("submit", addTodo);
    document.addEventListener("DOMContentLoaded", loadAllTodosToUI);
    secondCardBody.addEventListener("click", deleteTodo);
    filter.addEventListener("keyup",filterTodos);
    clearbutton.addEventListener("click",clearAllTodos);
}
function clearAllTodos() {
    if (confirm("Tümünü silmek istediğinize emin misiniz ?")){
        //Arayüzden Todoları temizleme
        while (todoList.firstElementChild!=null){
            todoList.removeChild(todoList.firstElementChild);
        }
    }
    localStorage.removeItem("todos");
}
function filterTodos(e) {
    const filtervalue=e.target.value.toLowerCase();
    const listItems=document.querySelectorAll(".list-group-item");
    
    listItems.forEach(function (listItem) {
            const text=listItem.textContent.toLowerCase();
            if  (text.indexOf(filtervalue)===-1){
            //BULAMADI
                listItem.setAttribute("style","display:none!important");
            }else{
                listItem.setAttribute("style","display:block");
            }
    });


}
function deleteTodo(e) {

    if (e.target.className === "fa fa-remove") {
        e.target.parentElement.parentElement.remove();
        deleteTodoFromStorage(e.target.parentElement.parentElement.textContent);
        showAlert("success", "Silme İşlemi Başarılı");
    }


}

function deleteTodoFromStorage(deleteTodo) {
    let todos = getTodosFromStorage();
    todos.forEach(function (todo,index) {
        if (todo===deleteTodo) {
                todos.splice(index,1);//Arrayden değeri silebiliriz.
        }
    });
    localStorage.setItem("todos",JSON.stringify(todos));
}


function loadAllTodosToUI() {
    let todos = getTodosFromStorage();
    todos.forEach(function (todo) {
        addTodoToUI(todo);
    });

}

function addTodo(e) {
    const newTodo = todoInput.value.trim();
    if (newTodo == "") {
        showAlert("danger", "Lütfen bir todo girin ");
    } else {
        addTodoToUI(newTodo);
        addTodoToStorage(newTodo);
        showAlert("success", "Başarılır bir şekilde eklediniz");
    }
    e.preventDefault();
}

//Storage ekleme fonksiyonu her yerde kullanıcağımız için
function getTodosFromStorage() {
    let todos;
    if (localStorage.getItem("todos") === null) {
        todos = [];
    } else {
        todos = JSON.parse(localStorage.getItem("todos"));
    }
    return todos;
}

function addTodoToStorage(newTodo) {
    let todos = getTodosFromStorage();
    todos.push(newTodo);
    localStorage.setItem("todos", JSON.stringify(todos));
}


function showAlert(type, message) {
    const alert = document.createElement("div");
    alert.className = `alert alert-${type}`;
    alert.textContent = message;
    console.log(alert);
    firstCardBody.appendChild(alert);
    //SetTimeout
    setTimeout(function () {
        alert.remove();
    }, 1000);
}


function addTodoToUI(newTodo) {  //string değerini list item olarak ekliycek
    //List item
    const listItem = document.createElement("li");
    //Link İtem
    const link = document.createElement("a");
    link.href = "#";
    link.className = "delete-item";
    link.innerHTML = "<i class = \"fa fa-remove\"></i>";

    listItem.className = "list-group-item list-group-item d-flex justify-content-between";


    //Text Node Ekleme
    listItem.appendChild(document.createTextNode(newTodo));
    //Link ekleme
    listItem.appendChild(link);
    // TodoLİst'E list item ekleme
    todoList.appendChild(listItem);
    todoInput.value = "";

}
